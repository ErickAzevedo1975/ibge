#ARQUIVO COM AS KEYWORD BÁSICAS QUE SERÃO UTILIZADAS EM "TODOS" OS TESTES

*** Settings ***
Resource                base.robot


*** Variables ***
#GERAL
${BASE_URL}                           https://servicodados.ibge.gov.br/api/v3
${URL_SITE}                           https://servicodados.ibge.gov.br
${BROWSER}                            Chromium
${headless}                           False
${OPTIONS}                            add_argument("--disable-dev-shm-usage"); add_argument("--no-sandbox"); add_experimental_option('excludeSwitches',['enable-logging'])
# ${OPTIONS}                            add_argument("--disable-dev-shm-usage"); add_argument("--headless"); add_argument("--no-sandbox"); add_experimental_option('excludeSwitches',['enable-logging'])
${TIME_WAIT}                          5
${WIDTH}                              1920
${HEIGHT}                             1080
${PATH_DATA}                          ../Test_Data
${TITLE_HOME}                         IBGE - API de serviço de dados

*** Keywords ***
Abrir site IBGE
    New Browser                     ${BROWSER}    headless=${headless}
    New Page                        ${URL_SITE}
    Get Title    ==                 ${TITLE_HOME}
    
Encerra Teste
    Take Screenshot
    Close Browser