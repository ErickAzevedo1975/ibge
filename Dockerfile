FROM ubuntu

ARG PROJECT_NAME

#update the image | install packages
RUN apt-get update

#install python
RUN apt-get install -y python3.9
RUN apt-get install -y python3-pip

#install robotframework and libraries
RUN pip3 install robotframework
RUN pip3 install robotframework-seleniumlibrary
RUN pip3 install robotframework-requests
#INCLUIR COMANDO DE INSTALAÇÃO DAS LIBRARIES UTILIZADAS NOS TESTES
